package com.example.shayan.taskapp.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.shayan.taskapp.Adapter.FriendsAdapter;
import com.example.shayan.taskapp.MainActivity;
import com.example.shayan.taskapp.R;
import com.example.shayan.taskapp.helper.FrontEngine;
import com.example.shayan.taskapp.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FriendsFragment extends BaseFragment implements View.OnClickListener, FriendsAdapter.ItemClickListener {

    private View rootView;
    public SwipeRefreshLayout swipeContainer;
    Context context = null;
    RecyclerView recyclerview;
    private ArrayList<UserModel> arrListCategories;// = new ArrayList<>();
    FriendsAdapter adapter;
    ProgressBar progressBarPro;
    Boolean isScrolling = false;
    int CurrentItems, TotalItems, ScrolledOutItems;
    public static int id = 0;
    TextView txtNoFound;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_main, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
        setRecyclerView();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.home_fragment), View.VISIBLE, R.mipmap.icon_menu, View.VISIBLE, R.mipmap.add_button);
        context = getContext();
    }

    private void initializeControls() {
        recyclerview = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        progressBarPro = (ProgressBar) rootView.findViewById(R.id.progressbar);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);

    }



    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                CurrentItems = recyclerView.getLayoutManager().getChildCount();
                TotalItems = recyclerView.getLayoutManager().getItemCount();
                ScrolledOutItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                if (isScrolling && (CurrentItems + ScrolledOutItems == TotalItems)) {
                    isScrolling = false;

                    //dataFetch
                    fetchData();
                }


            }
        });
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code here
//
                setData();
                adapter.notifyDataSetChanged();
                progressBarPro.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "Works!", Toast.LENGTH_LONG).show();
                // To keep animation for 4 seconds
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        // Stop animation (This will be after 3 seconds)
                        swipeContainer.setRefreshing(false);
                    }
                }, 1000); // Delay in millis
            }
        });

    }

    public void setRecyclerView()

    {
        setData();

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerview.setLayoutManager(mLayoutManager);

        adapter = new FriendsAdapter(getActivity(), arrListCategories);
        recyclerview.setAdapter(adapter);

        adapter.setClickListener(this);
    }

    private void fetchData() {
        progressBarPro.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                UserModel promodel = new UserModel();
                UserModel newmodel2 = new UserModel();
                UserModel newmodel3 = new UserModel();
                UserModel newmodel4 = new UserModel();
                UserModel newmodel5 = new UserModel();
                UserModel newmodel6 = new UserModel();
                UserModel newmodel7 = new UserModel();
                UserModel newmodel8 = new UserModel();

//                newmodel.setName("Death.");
//                newmodel.setPropic("http://img04.deviantart.net/699c/i/2012/134/9/f/death_from_darksiders_by_artisticphenom-d4zs0ae.jpg");
//                newmodel.setBio("This is Death");
//                newmodel.setEmail("death@thqnordic.com");
//                newmodel.setPhone("123-401-41212");
//                newmodel.setFollowers("400K");
//                newmodel.setFollowing("12");
//                newmodel.setTwitter("@Death");
//                newmodel3.setName("War");
//                newmodel3.setPropic("http://images2.fanpop.com/image/photos/10400000/War-darksiders-10468476-1024-1491.jpg");
//                newmodel3.setBio("On For War ");
//                newmodel3.setEmail("war@thqnordic.com");
//                newmodel3.setPhone("12131-213");
//                newmodel3.setFollowers("900K");
//                newmodel3.setFollowing("121");
//                newmodel3.setTwitter("@War");
//                newmodel4.setName("Strife");
//                newmodel4.setPropic("https://i.pinimg.com/originals/79/91/f9/7991f9fd8dace896734c423d01a9d488.jpg");
//                newmodel4.setBio("Strifiest Strife");
//                newmodel4.setEmail("Strife@thqnordic.com");
//                newmodel4.setPhone("0021331-31414131");
//                newmodel4.setFollowers("900K");
//                newmodel4.setFollowing("151");
//                newmodel4.setTwitter("@Strife");
//                newmodel = new UserModel("Robert Downey Jr", "https://nyppagesix.files.wordpress.com/2017/08/la_premiere_of__spider-man-_homecoming_.jpg?quality=90&strip=all&w=618&h=410&crop=1", "I'm IronMan", "Millionaire", "ironman_tony@marvel.com", "@IronManStark", "123-401-4121", "400K", "12", id);
//                newmodel2 = new UserModel("Goaty McGoatFace", "https://www.veterantv.com/wp-content/uploads/2017/08/f5d784aa1eabbde15ba5e2d90c3ba828.jpg", "I'm a Goat", "Millionaire philanthropist", "goating_away@goatech.com", "@GoatyMcGoat", "1234-40113-412131", "1.4M", "3", id);
//                newmodel3 = new UserModel("Bennedict CumberBatch", "https://m.media-amazon.com/images/M/MV5BMjE0MDkzMDQwOF5BMl5BanBnXkFtZTgwOTE1Mjg1MzE@._V1_UY317_CR2,0,214,317_AL_.jpg", "Smaug & Shelock ", "Smaug & Shelock ", "ironman_tony@marvel.com", "@IronManStark", "123-401-4121", "400K", "12", id);
//                newmodel4 = new UserModel("Robert Downey Jr  .1", "https://nyppagesix.files.wordpress.com/2017/08/la_premiere_of__spider-man-_homecoming_.jpg?quality=90&strip=all&w=618&h=410&crop=1", "I'm IronMan", "Millionaire", "benndict_cumberbatch@gmail.com", "@Bennedict", "12131-414212131", "900K", "121", id);

//                newmodel5 = new UserModel("Jennifer Anniston", "https://m.media-amazon.com/images/M/MV5BNjk1MjIxNjUxNF5BMl5BanBnXkFtZTcwODk2NzM4Mg@@._V1_UY317_CR3,0,214,317_AL_.jpg", "Offical accoount", "Offical accoount", "jennifer_anniston@gmail.com", "@jennifer", "0021331-414131", "700K", "152", id);
                int apple =TotalItems;
                for (int i = TotalItems; i < (TotalItems+8); i++) {
                    promodel = new UserModel("Furry", "https://img.fireden.net/v/image/1493/75/1493757983247.png", "Angry Furry", "Millionaire", "furry@thqNordic.com", "@Furry", "1234-40112313-412131", "1.3M", "3", i);
                    arrListCategories.add(promodel);
                    adapter.notifyDataSetChanged();
                    progressBarPro.setVisibility(View.GONE);
                }

            }
        }, 3000);

    }


    public void setData() {
        arrListCategories = new ArrayList<>();
        UserModel newmodel = new UserModel();
//        UserModel newmodel2 = new UserModel();
//        UserModel newmodel3 = new UserModel();
//        UserModel newmodel4 = new UserModel();
//        UserModel newmodel5 = new UserModel();
//        /newmodel2 = new UserModel("Goaty McGoatFace", "https://www.veterantv.com/wp-content/uploads/2017/08/f5d784aa1eabbde15ba5e2d90c3ba828.jpg", "I'm a Goat", "Millionaire philanthropist", "goating_away@goatech.com", "@GoatyMcGoat", "1234-40113-412131", "1.4M", "3", id);
//        newmodel3 = new UserModel("Bennedict CumberBatch", "https://m.media-amazon.com/images/M/MV5BMjE0MDkzMDQwOF5BMl5BanBnXkFtZTgwOTE1Mjg1MzE@._V1_UY317_CR2,0,214,317_AL_.jpg", "Smaug & Shelock ", "Smaug & Shelock ", "ironman_tony@marvel.com", "@IronManStark", "123-401-4121", "400K", "12", id);
//        newmodel4 = new UserModel("Robert Downey Jr  .1", "https://nyppagesix.files.wordpress.com/2017/08/la_premiere_of__spider-man-_homecoming_.jpg?quality=90&strip=all&w=618&h=410&crop=1", "I'm IronMan", "Millionaire", "benndict_cumberbatch@gmail.com", "@Bennedict", "12131-414212131", "900K", "121", id);
//
//        newmodel5 = new UserModel("Jennifer Anniston", "https://m.media-amazon.com/images/M/MV5BNjk1MjIxNjUxNF5BMl5BanBnXkFtZTcwODk2NzM4Mg@@._V1_UY317_CR3,0,214,317_AL_.jpg", "Offical accoount", "Offical accoount", "jennifer_anniston@gmail.com", "@jennifer", "0021331-414131", "700K", "152", id);
//

        for (int i = 0; i < 8; i++) {
            newmodel = new UserModel("Robert Downey Jr", "https://nyppagesix.files.wordpress.com/2017/08/la_premiere_of__spider-man-_homecoming_.jpg?quality=90&strip=all&w=618&h=410&crop=1", "I'm IronMan", "Millionaire", "ironman_tony@marvel.com", "@IronManStark", "123-401-4121", "400K", "12", i);
            arrListCategories.add(newmodel);

        }

//         arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);

        FrontEngine.getInstance().setArrListCategories(arrListCategories);

       /* if(arrListCategories.size()>0){
            txtNoFound.setVisibility(View.GONE);
        }else {
            txtNoFound.setVisibility(View.VISIBLE);
        }*/


    }


    @Override
    public void onClick(View v) {

    }

    public void showDialog(int position) {
        final Dialog dialog = new Dialog(getContext(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        dialog.setContentView(R.layout.profile_pic_dialog);
        dialog.setTitle("");

        // set the custom dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.dial_img);
        ImageView cross = (ImageView) dialog.findViewById(R.id.cross_img);
        Picasso.get().load(arrListCategories.get(position).getPropic()).into(image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onItemClick(View view, int position) {
//
        Bundle bundle = new Bundle();
        bundle.putSerializable("123", arrListCategories.get(position));
        Fragment fragment;
        fragment = new ProfileFragment();
        fragment.setArguments(bundle);
        ((MainActivity) getActivity()).replaceFragment(fragment, getActivity());


    }

    public void onImageClick(View view, int position) {
        String apple = getTag();
        if (apple == getTag()) {
            showDialog(position);
        }


    }


}

