package com.example.shayan.taskapp.fragments;

import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Created by Development on 5/27/2017.
 */

public class BaseFragment extends Fragment {

    public BaseFragment(){

    }

    //edit field action done or next
    public void actionEditText(final EditText editText, final ScrollView scroll) {

        editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_DONE || i == EditorInfo.IME_ACTION_UNSPECIFIED) {

                    return true;
                }
                return false;
            }

        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {

                } else {
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

    }
    //edit field action done or next clos




}
