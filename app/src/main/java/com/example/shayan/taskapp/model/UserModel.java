package com.example.shayan.taskapp.model;

import android.widget.ImageView;

import java.io.Serializable;


public class UserModel implements Serializable {


    private String name, bio, status, email, twitter, phone, propic, followers, following;
    private int id;
    public UserModel() {
    }


    public UserModel(String name, String propic, String status, String bio, String email, String twitter, String phone, String followers, String following,int id) {
        super();
        this.name = name;
        this.bio = bio;
        this.propic = propic;
        this.status = status;
        this.email = email;
        this.twitter = twitter;
        this.phone = phone;
        this.followers = followers;
        this.following = following;
        this.id= id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPropic() {
        return propic;
    }

    public void setPropic(String propic) {
        this.propic = propic;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }
}

