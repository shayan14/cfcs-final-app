package com.example.shayan.taskapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;


import android.content.Context;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.example.shayan.taskapp.Adapter.DrawerAdapter;
import com.example.shayan.taskapp.Utils.PermissionUtils;
import com.example.shayan.taskapp.fragments.AboutFragment;
import com.example.shayan.taskapp.fragments.AddCartItemFragment;
import com.example.shayan.taskapp.fragments.ContactUsFragment;
import com.example.shayan.taskapp.fragments.FriendsFragment;
import com.example.shayan.taskapp.fragments.ProfileFragment;
import com.example.shayan.taskapp.helper.Constants;
import com.example.shayan.taskapp.helper.FrontEngine;
import com.example.shayan.taskapp.model.UserModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextDetector;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.example.shayan.taskapp.helper.FrontEngine.pictureImagePath;

public class MainActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    //Header
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public int PERMISSION_CAPTURE_INT = 1000;
    private static final int PERMISSION_REQUEST = 33;
    ImageView ProfilePic;
    private static final String MODEL_FILE = "file:///android_asset/tensorflow_inception_graph.pb";
    private static final String LABEL_FILE =
            "file:///android_asset/imagenet_comp_graph_label_strings.txt";

    FirebaseVisionImage image;
    TextView textView;
    FirebaseVisionTextDetector detector;

    public static String key;

    public static LinearLayout headerLl, llMenu1, llMenu2;
    public static TextView tv_title, tvMenu3, tvMenu4;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    private boolean isRequested = false;
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    public static ImageView ivMenu1, ivMenu2;
    private static final int PERMISSION_REQUEST_CODE = 200;


    //Drawer
    public static LinearLayout ll_profile;
    TextView user_name;
    TextView DrawerName;
    TextView DrawerEmail;
    ImageView img_profile;
    UserModel user = null;
    List<String> permissionList;
    ProgressBar pbar;
    private DrawerLayout drawer;
    private LinearLayout ll_edit_profile;
    ChangeProfilePicDialog dialog;

    private String fragmentName = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        auth = FirebaseAuth.getInstance();
        user = new UserModel();
        user.setName(auth.getCurrentUser().getEmail());
      permissionList=  PermissionUtils.checkAndRequestPermissions(MainActivity.this);
        //main logic or main code
        initializeControls();
        setOnClickListeners();
        setMenuBarAndLoadItems();
        SelectItem(0);
        checkpermission();
        permissions(permissionList);
        // . write your main code to execute, It will execute if the permission is already given.

    }

    private boolean permissions(List<String> listPermissionsNeeded) {

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void checkpermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(MainActivity.this)) {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//

                    } else {

                        requestPermissions(FrontEngine.permissionsWriteStorage, PERMISSION_REQUEST);
                    }

                }

            }
        }
        //
//        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR);
//        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//            // User may have declined earlier, ask Android if we should show him a reason
//            if (ActivityCompat.shouldShowRequestPermissionRationale(MainMenuActivity.this, Manifest.permission.WRITE_CALENDAR)) {
//                // show an explanation to the user
//                // Good practise: don't block thread after the user sees the explanation, try again to request the permission.
//            } else {
//                // request the permission.
//                // CALLBACK_NUMBER is a integer constants
//                requestPermissions(FrontEngine.permissions, PERMISSION_REQUEST);
////                requestPermissions(MainMenuActivity.this, FrontEngine.permissions, PERMISSION_REQUEST);
//                // The callback method gets the result of the request.
//            }
//        } else {
//// got permission use it
//        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");

                } else {
                    Log.e("Permission", "Denied");
                    if (!isRequested) {
                        isRequested = true;
                        MainActivity.this.requestPermissions(FrontEngine.permissionsWriteStorage, PERMISSION_REQUEST);

                    }
                }
                return;
            }

        }
    }


    public void initializeControls() {
        dialog = new ChangeProfilePicDialog(MainActivity.this, this);
        tv_title = (TextView) findViewById(R.id.tv_title);
        headerLl = (LinearLayout) findViewById(R.id.headerLl);

        llMenu1 = (LinearLayout) findViewById(R.id.llMenu1);
        llMenu2 = (LinearLayout) findViewById(R.id.llMenu2);

        ivMenu1 = (ImageView) findViewById(R.id.ivMenu1);
        ivMenu2 = (ImageView) findViewById(R.id.ivMenu2);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        textView = (TextView) findViewById(R.id.textView);

    }

    public void setOnClickListeners() {

        //Header Buttons Left
        llMenu1.setOnClickListener(this);
        llMenu2.setOnClickListener(this);
        ivMenu1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Fragment fragmentNav = null;
        switch (v.getId()) {
            case R.id.llMenu1:
                openAndCloseDrawer(true);
                break;
            case R.id.ivMenu1:
                openAndCloseDrawer(true);
                break;
            case R.id.drawer_profile_image:
                dialog.show();
                break;
            case R.id.name_drawer:
                fragmentNav = new ProfileFragment();
                break;
            case R.id.email_drawer:
                fragmentNav = new ProfileFragment();
                break;
            case R.id.ivMenu2:
//                Todo Open cart Activity here
                break;

        }
    }

    public void openAndCloseDrawer(boolean menu) {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.close_menu_rotate_animation);
//            llMenu1.startAnimation(startRotateAnimation);
            drawer.closeDrawer(GravityCompat.START);
        } else if (menu) {
//            Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.open_menu_rotate_animation);
//            llMenu1.startAnimation(startRotateAnimation);
            drawer.openDrawer(GravityCompat.START);
        }

    }

    public void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void setMenuBarAndLoadItems() {
        ArrayList<String> dataList = new ArrayList<String>();
        ArrayList<Integer> dataicon = new ArrayList<Integer>();
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);
        ProfilePic = (ImageView) findViewById(R.id.drawer_profile_image);
        DrawerName = (TextView) findViewById(R.id.name_drawer);
        DrawerEmail = (TextView) findViewById(R.id.email_drawer);
        Glide.with(this).load(user.getPropic()).into(ProfilePic);
        DrawerName.setText("Welcome to CFCS");
        DrawerEmail.setText(auth.getCurrentUser().getEmail());
        DrawerName.setOnClickListener(this);
        DrawerEmail.setOnClickListener(this);

//        Uri apple=FrontEngine.imagePathUri;
//        Glide.with(this).load().into(ProfilePic);
        // mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
        // GravityCompat.START);


        dataList.add("Home"); // 0
        dataicon.add(R.mipmap.home);
        dataList.add("All Users"); // 1
        dataicon.add(R.mipmap.location);
        dataList.add("Contact Us");// 2
        dataicon.add(R.mipmap.contact_us);
//        dataList.add("Location"); // 3
//        dataicon.add(R.mipmap.location);
//        dataList.add("Favourite"); // 4
//        dataicon.add(R.mipmap.favourite);
        dataList.add("About Us");//3
        dataicon.add(R.mipmap.about);
        dataList.add("Something");//4
        dataicon.add(R.mipmap.changelanguage);
        dataList.add("Sign Out"); // 5
        dataicon.add(R.mipmap.exit);

        String lang = pref.getString(Constants.LOCALE, "th");
        if (lang == "th" && isThai) {
            dataList.set(4, "Eng");
        } else
            dataList.set(4, "ไทย");

        DrawerAdapter adapter1 = new DrawerAdapter(this, dataList, dataicon);


        ArrayAdapter adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.menu_item1, dataList);

        mDrawerList.setAdapter(adapter1);

        mDrawerList.setOnItemClickListener(this);
        ProfilePic.setOnClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SelectItem(position);
//        Toast.makeText(this, "List Item " + position, Toast.LENGTH_SHORT).show();
    }

    public void SelectItem(int position) {
        Fragment fragmentNav = null;
        switch (position) {
            case 0: // 0

                closeDrawer();
                fragmentNav = new AddCartItemFragment();

                break;

            case 1: // 1

                closeDrawer();
                fragmentNav = new FriendsFragment();

                break;

            case 2: // 2

                closeDrawer();
                fragmentNav = new ContactUsFragment();

                break;

            case 3: // 3

                closeDrawer();
                fragmentNav = new AboutFragment();
                break;
//            case 4: // Settings
//
//                closeDrawer();
////                fragmentNav = new TextbooksFragment();
//
//                break;

            case 4: // 4

                closeDrawer();
                ChangeLanguageDialog(this);
                break;


            case 5: // 5

                closeDrawer();
                signout();

//   fragmentNav = new AboutFragment();
                break;


        }

//        if (position != 6 || position != 4) {
        replaceFragment(fragmentNav, this);
//        }

    }

    private void signout() {
        auth.signOut();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);

    }

    public void replaceFragment(Fragment currentFragment, FragmentActivity fragmentActivity) {
        try {
            objGlobalHelperNormal.hideSoftKeyboard(fragmentActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (currentFragment != null && (!fragmentName.equals(currentFragment.getClass().getName()))) {
            llMenu1.setOnClickListener(null);
            llMenu2.setOnClickListener(null);
            try {
                String backStateName = currentFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();

                boolean fragmentPopped = false;
                if (!backStateName.equals("com.appocta.a2ulearn.fragments.LessonOneFragment")) {
                    fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                }

//                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

                if (!fragmentPopped) { //fragment not in back stack, create it.
                    FragmentTransaction ft = manager.beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    ft.replace(R.id.content_frame, currentFragment, backStateName);
                    ft.addToBackStack(backStateName);
                    ft.commit();
                }
//                footerUIChange(backStateName);
                fragmentName = backStateName;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
//        FrontEngine.getInstance().objLocationsModel = null;
//        FrontEngine.getInstance().objPropertyModel = null;
        try {
            objGlobalHelperNormal.hideSoftKeyboard(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            } else {
//                fragmentName = getSupportFragmentManager().getFragments().
//                        get(getSupportFragmentManager().getBackStackEntryCount()-1).getClass().getName();
                String myFragmentName = getSupportFragmentManager().getBackStackEntryAt(
                        getSupportFragmentManager().getBackStackEntryCount() - 2).getName();
                if (!myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonOneFragment")
                        && !myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonTwoFragment")
                        && !myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonThreeFragment")
                        && !myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonFourFragment")
                        && !myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonResultFragment")) {
                    fragmentName = myFragmentName;
                    super.onBackPressed();
                } else {
//                    if (FrontEngine.getInstance().getLessonType().equals("home")) {
//                        replaceFragment(new LessonListFragment(), this);
//                    } else if (FrontEngine.getInstance().getLessonType().equals("goal")) {
//                        replaceFragment(new GoalsFragment(), this);
//                    }
                }
            }
        }

    }

    public void ChangeLanguageDialog(final Context context) {
        final Dialog dialog;

        final TextView title, content, hint, EnglishBtn, ThaiBtn;
        ImageView cross;
        dialog = new Dialog(context,
                android.R.style.Theme_Light_NoTitleBar);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().clearFlags(
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);//

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        dialog.setCancelable(false);

        dialog.setContentView(R.layout.dialog_change_language);

        title = (TextView) dialog
                .findViewById(R.id.dialog_title);
        EnglishBtn = (TextView) dialog
                .findViewById(R.id.btn_english);
        ThaiBtn = (TextView) dialog
                .findViewById(R.id.btn_thai);
        cross = (ImageView) dialog
                .findViewById(R.id.dialog_cross);


        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        EnglishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isThai = false;
                FrontEngine.getInstance().changeLang("en", MainActivity.this);
                MainActivity.this.getSharedPreferences(Constants.OAUTH_PREF, MODE_PRIVATE).edit().putString(Constants.LOCALE, "en").commit();
                Intent MainActivity = new Intent(MainActivity.this,
                        MainActivity.class);
                MainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                MainActivity.this.startActivity(MainActivity);
            }


        });
        ThaiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isThai = true;
                FrontEngine.getInstance().changeLang("th", MainActivity.this);
                MainActivity.this.getSharedPreferences(Constants.OAUTH_PREF, MODE_PRIVATE).edit().putString(Constants.LOCALE, "th").commit();
                Intent MainActivity = new Intent(MainActivity.this,
                        MainActivity.class);
                MainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                MainActivity.this.startActivity(MainActivity);
            }
        });

        dialog.show();

    }


    public void initialWorkForAllFragments(Context c, String title,
                                           int one, int oneImg, int two, int twoImg) {
        tv_title.setText(title);
        ivMenu1.setVisibility(one);
        ivMenu1.setOnClickListener(this);
        ivMenu2.setVisibility(View.INVISIBLE);
        ivMenu1.setImageResource(oneImg);
        ivMenu2.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PERMISSION_CAPTURE_INT) {

                File imgFile = new File(pictureImagePath);
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    image = FirebaseVisionImage.fromBitmap(myBitmap);
                    detector = FirebaseVision.getInstance()
                            .getVisionTextDetector();
                    Task<FirebaseVisionText> result =
                            detector.detectInImage(image)
                                    .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                                        @Override
                                        public void onSuccess(FirebaseVisionText firebaseVisionText) {
                                            // Task completed successfully
                                            textView.setText("START\n");
                                            for (FirebaseVisionText.Block block : firebaseVisionText.getBlocks()) {
                                                Rect boundingBox = block.getBoundingBox();
                                                Point[] cornerPoints = block.getCornerPoints();
                                                String text = block.getText();
                                                textView.append(text);
                                                switch (text) {
                                                    case "Dettol":
                                                        key = "Dettol";
                                                        Intent newActivity1 = new Intent(MainActivity.this, CartActivity.class);
                                                        newActivity1.putExtra("key", "Dettol");
                                                        startActivity(newActivity1);

                                                        break;
                                                    case "1kg":
                                                        key = "Ariel";
                                                        Intent newActivity2 = new Intent(MainActivity.this, CartActivity.class);
                                                        newActivity2.putExtra("key", "Ariel");
                                                        startActivity(newActivity2);

                                                        break;
                                                    case "Vanilla":
                                                        key = "wafer";
                                                        Intent newActivity3 = new Intent(MainActivity.this, CartActivity.class);
                                                        newActivity3.putExtra("key", "wafer");
                                                        startActivity(newActivity3);

                                                        break;

                                                     case "PEPSI":
                                                    key ="Pepsi";
                                                    Intent newActivity4 = new Intent(MainActivity.this, CartActivity.class);
                                                    newActivity4.putExtra("key","Pepsi");
                                                    startActivity(newActivity4);

                                                    break;
                                                    case "500g":
                                                    key ="ariel500g";
                                                    Intent newActivity5 = new Intent(MainActivity.this, CartActivity.class);
                                                    newActivity5.putExtra("key", "ariel500g");
                                                    startActivity(newActivity5);

                                                    break;
                                                    case "OLIVE":
                                                    key ="oliveOil";
                                                    Intent newActivity6 = new Intent(MainActivity.this, CartActivity.class);
                                                    newActivity6.putExtra("key", "oliveOil");
                                                    startActivity(newActivity6);

                                                    break;
                                                    case "Peaceful\nNights":
                                                    key ="Mortine";
                                                    Intent newActivity7 = new Intent(MainActivity.this, CartActivity.class);
                                                    newActivity7.putExtra("key", "Mortine");
                                                    startActivity(newActivity7);
                                                    break;
                                                    case "DRY\nProtect":
                                                    key ="deo";
                                                    Intent newActivity8 = new Intent(MainActivity.this, CartActivity.class);
                                                    newActivity8.putExtra("key", "deo");
                                                    startActivity(newActivity8);

                                                    break;
case "beautiful":
                                                    key ="Haircolor";
                                                    Intent newActivity9 = new Intent(MainActivity.this, CartActivity.class);
                                                    newActivity9.putExtra("key", "haircolor");
                                                    startActivity(newActivity9);

                                                    break;
case "Alluring":
                                                    key ="Powder";
                                                    Intent newActivity10 = new Intent(MainActivity.this, CartActivity.class);
                                                    newActivity10.putExtra("key", "Powder");
                                                    startActivity(newActivity10);

                                                    break;
case "silky black":
                                                    key ="Silky Black";
                                                    Intent newActivity11 = new Intent(MainActivity.this, CartActivity.class);
                                                    newActivity11.putExtra("key", "Silky Black");
                                                    startActivity(newActivity11);

                                                    break;


                                                }
                                            }

//                                            Toast.makeText(getApplicationContext(), key, Toast.LENGTH_SHORT).show();

                                        }
                                    })
                                    .addOnFailureListener(
                                            new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    //Code for on failure
                                                    Toast.makeText(MainActivity.this, "Image Recognition Failed  " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                }
                Toast.makeText(getApplicationContext(), "Failed To Detect Image", Toast.LENGTH_SHORT).show();


                //                    try {
//                     probit=   rotateimage(myBitmap);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    dialog.ProfilePic.setImageBitmap(myBitmap);
//                    Bitmap ThumbImage = ThumbnailUtils.extractThumbnail(myBitmap, 200, 200);

//                    ProfilePic.setImageBitmap(ThumbImage);
                    /*            if (data != null && data.getExtras() != null) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                Bitmap resized = Bitmap.createScaledBitmap(imageBitmap, 300, 300, true);
                dialog.ProfilePic.setImageBitmap(imageBitmap);}*/
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            File imgFile = new File(pictureImagePath);
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                Bitmap probit = null;
//                    try {
//                        probit = rotateimage(myBitmap);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    dialog.ProfilePic.setImageBitmap(myBitmap);
//                    Bitmap ThumbImage = ThumbnailUtils.extractThumbnail(myBitmap, 200, 200);

//                    ProfilePic.setImageBitmap(ThumbImage);awaz a
            }
        }

    }


    private Bitmap rotateimage(Bitmap myBitmap) throws IOException {
        ExifInterface ei = null;
//        ei=new ExifInterface(String.valueOf(myBitmap=BitmapFactory.decodeFile(FrontEngine.imagePathUri.getPath())));

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        Bitmap rotatedBitmap = null;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(myBitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(myBitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(myBitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = myBitmap;
        }
        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
}

