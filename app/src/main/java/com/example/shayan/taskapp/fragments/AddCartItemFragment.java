package com.example.shayan.taskapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.example.shayan.taskapp.CartActivity;
import com.example.shayan.taskapp.MainActivity;
import com.example.shayan.taskapp.R;
import com.example.shayan.taskapp.Utils.PermissionUtils;
import com.example.shayan.taskapp.helper.FrontEngine;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.example.shayan.taskapp.helper.FrontEngine.pictureImagePath;


public class AddCartItemFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    String lang=null;
    private Button AddItem,CrashButton;
    android.content.res.Configuration config;
    Context context;
    int PERMISSION_CAPTURE_INT = 1000;


    TextView txtInfo;

    public AddCartItemFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_cart_item, container,
                false);
        context = getActivity();
        initialWork();
        initializeControls();
        setOnClickListeners();

        return rootView;
    }




    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                "Add Items To Cart", View.VISIBLE, R.mipmap.icon_menu, 1, R.mipmap.cart);

    }


    private void initializeControls() {
        txtInfo = (TextView) rootView.findViewById(R.id.add_item);
        AddItem = (Button) rootView.findViewById(R.id.cart_add);
        CrashButton = (Button) rootView.findViewById(R.id.crash_button);
//        config.loc
    }


    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);

        AddItem.setOnClickListener(this);
        CrashButton.setOnClickListener(this);
        ((MainActivity) getActivity()).ivMenu2.setOnClickListener(this);

//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    public void setData() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).openAndCloseDrawer(true);
                break;

            case R.id.ivMenu2:
                openCartActivity();
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
            case R.id.cart_add:
                openCameraIntent();
                Crashlytics.getInstance().crash();

                break;
            case R.id.crash_button:
                openCartActivity();
                Crashlytics.getInstance().crash();
                break;
        }
        }

    private void openCartActivity() {
        Intent intent = new Intent(getActivity(), CartActivity.class);
        startActivity(intent);



    }

    private void openCameraIntent() {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = timeStamp + ".jpg";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
            File file = new File(pictureImagePath);
            Uri outputFileUri = FileProvider.getUriForFile(context, "com.example.shayan.taskapp.provider", file);
            FrontEngine.imagePathUri = outputFileUri;
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            ((Activity) context).startActivityForResult(cameraIntent, PERMISSION_CAPTURE_INT);
        }

    }


