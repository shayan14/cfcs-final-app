package com.example.shayan.taskapp.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shayan.taskapp.R;
import com.example.shayan.taskapp.model.FeedModel;
import java.util.ArrayList;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {

    private ArrayList<FeedModel> mData;
    private LayoutInflater mInflater;
    Context context;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public FeedAdapter(Context context, ArrayList<FeedModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_news_feed, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int likes=mData.get(position).getLikes();
        int comments=mData.get(position).getComments();
        int propic=mData.get(position).getPropic();
        int postpic=mData.get(position).getPostpic();
        String name=mData.get(position).getName();
        String time=mData.get(position).getTime();
        String status=mData.get(position).getStatus();
        holder.tv_name.setText(""+name);
        holder.tv_time.setText(time);   
        holder.tv_likes.setText(""+likes);
        holder.tv_comments.setText(comments + " comments");
        holder.tv_status.setText(status);
        holder.imgView_postPic.setImageResource(postpic);
        holder.imgView_proPic.setImageResource(propic);


    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_name, tv_time, tv_likes, tv_comments, tv_status;
        ImageView imgView_proPic, imgView_postPic;
        ViewHolder(View itemView) {
            super(itemView);
            tv_name=itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_likes = itemView.findViewById(R.id.tv_like);
            tv_comments= itemView.findViewById(R.id.tv_comment);
            tv_status = itemView.findViewById(R.id.tv_status);
            imgView_proPic= itemView.findViewById(R.id.imgView_proPic);
            imgView_postPic=itemView.findViewById(R.id.imgView_postPic);
            imgView_postPic.setOnClickListener(this);
            imgView_postPic.setTag("baaa");
          /*  itemView.setOnClickListener(this);
            myImgView.setOnClickListener(this);
            myTextView.setOnClickListener(this);
            linearLayout.setOnClickListener(this);
            myImgView.setTag("Image");
            myTextView.setTag("Text");
            linearLayout.setTag("Entire Layout");
       */ }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    FeedModel getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}


