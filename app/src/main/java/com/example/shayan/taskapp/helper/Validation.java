package com.example.shayan.taskapp.helper;

import android.widget.EditText;

import java.util.regex.Pattern;

public class Validation {

    // Regular Expression
    // you can change the expression based on your need
    public String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public String PHONE_REGEX = "\\d{4}-\\d{7}";
    public String DATE_REGEX = "\\d{4}-\\d{2}-\\d{2}";
    public String PIN_REGEX = "\\d{4}";
    public String NAME_REGEX = "^[a-z0-9_-]{3,15}$";
    public String USERNAME_REGEX = "^.{3,15}$";
    public String PASSWORD_REGEX = "^.{6,12}$";
    public String LICENSE_NO_REGEX = "^.{5,20}$";

    // Error Messages
    public String REQUIRED_MSG = "Required Field";
    public String EMAIL_MSG = "Invalid Email Format";
    public String PHONE_MSG = "####-#######";
    public String DATE_MSG = "YYYY-MM-DD";
    public String PIN_MSG = "4digits";
    public String USERNAME_MSG = "Length limit should in between 3-15";
    public String NAME_MSG = "Characters limit should in between 3-15";
    public String PASSWORD_MSG = "Password length between 6-20";
    public String LICENSE_NO_MSG = "License number length should be greater than 5";
    private static Validation objValidation;

    private Validation() {

    }

    public static Validation getInstance() {
        if(objValidation == null){
            objValidation = new Validation();
        }
        return objValidation;
    }

    // call this method when you need to check email validation
    public boolean isEmailAddress(EditText editText, boolean required) {
        return isValid(editText, EMAIL_REGEX, EMAIL_MSG, required);
    }

    // call this method when you need to check email validation
    public boolean isPassword(EditText editText, boolean required) {
        return isValid(editText, PASSWORD_REGEX, PASSWORD_MSG, required);
    }

    // call this method when you need to check email validation
    public boolean isName(EditText editText, boolean required) {
        return isValid(editText, USERNAME_REGEX, USERNAME_MSG, required);
    }

    public boolean isPin(EditText editText, boolean required) {
        return isValid(editText, PIN_REGEX, PIN_MSG, required);
    }

    public boolean isDate(EditText editText, boolean required) {
        return isValid(editText, DATE_REGEX, DATE_MSG, required);
    }

    // call this method when you need to check phone number validation

    public boolean isPhoneNumber(EditText editText, boolean required) {
        return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
    }

    public boolean isEditText(EditText editText, String regex, String msg, boolean required) {
        return isValid(editText, regex, msg, required);
    }

    // return true if the input field is valid, based on the parameter passed
    public boolean isValid(EditText editText, String regex,
                           String errMsg, boolean required) {

        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);

        // text required and editText is blank, so return false
        if (required && !hasText(editText))
            return false;

        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
            editText.setError(errMsg);
            return false;
        }
        ;

        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    public boolean hasText(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);
        editText.requestFocus();

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(REQUIRED_MSG);
            return false;
        }

        return true;
    }
}

