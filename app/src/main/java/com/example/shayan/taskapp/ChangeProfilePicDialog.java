package com.example.shayan.taskapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shayan.taskapp.helper.FrontEngine;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.shayan.taskapp.helper.FrontEngine.pictureImagePath;

public class ChangeProfilePicDialog extends Dialog implements View.OnClickListener {

    private Context context;
    public ImageView ProfilePic, Cross;
    Activity activity;
    private Button ChangePic, CropPic;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    int PERMISSION_CAPTURE_INT = 1000;


    private Handler mHandler = null;
    private int type;


    public ChangeProfilePicDialog(Context context,Activity activity) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.context = context;
        this.activity=activity;
    }


    public ChangeProfilePicDialog(BaseActivity activity, int style) {
        super(activity, style);
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullScreenCode();
        setContentView(R.layout.drawer_dialog);
        initViews();

//        ToDO pick up from google link
// createImageFile();

    }

    protected void fullScreenCode() {

        Window window = this.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    }

    private void initViews() {
        ProfilePic = (ImageView) findViewById(R.id.drawer_profile_image);
        ProfilePic.setImageURI(Uri.parse("file://mnt/sdcard/d2.jpg"));
        Cross = (ImageView) findViewById(R.id.cross_img);
        ChangePic = (Button) findViewById(R.id.change_pic);
        CropPic = (Button) findViewById(R.id.crop_pic);
        ChangePic.setOnClickListener(this);
        CropPic.setOnClickListener(this);
        Cross.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.change_pic:
                openCameraIntent();
                break;
            case R.id.crop_pic:
                Crop.of(FrontEngine.imagePathUri,FrontEngine.imagePathUri).asSquare().start(activity);
                break;
            case R.id.cross_img:
                    dismiss();
                    break;
//        }
        }

//    private void changepicture() {
//    }

    }


    private void cropimageintent() {
    }

    private void openCameraIntent() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp + ".jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
        File file = new File(pictureImagePath);
        Uri outputFileUri = FileProvider.getUriForFile(context, "com.example.shayan.taskapp.provider", file);
        FrontEngine.imagePathUri = outputFileUri;
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        ((Activity) context).startActivityForResult(cameraIntent, PERMISSION_CAPTURE_INT);
    }


//    private void captureimage() {
//
//        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//        context.startActivity(intent);
//
//    }
//
 /*   private void dialogdismiss() {
        AlertDialog optionDialog = new AlertDialog.Builder(context).create();
    }
*/
}
