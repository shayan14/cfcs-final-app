package com.example.shayan.taskapp.model;


public class ItemModel {

    int price,quantity;
    String name;
    String variant;
    int count = 0;



    public ItemModel(){

    }


    public ItemModel(String name, int quantity, int price,String variant,int count) {
        this.name = name;
        this.quantity=quantity;
        this.price=price;
        this.variant=variant;
        this.count=count;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
