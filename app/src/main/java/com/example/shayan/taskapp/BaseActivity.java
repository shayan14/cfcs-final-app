package com.example.shayan.taskapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.shayan.taskapp.helper.Constants;
import com.example.shayan.taskapp.helper.FrontEngine;
import com.example.shayan.taskapp.helper.GlobalHelperNormal;
import com.example.shayan.taskapp.helper.SharedPreference;
import com.example.shayan.taskapp.helper.Validation;


public class BaseActivity extends AppCompatActivity {

    public GlobalHelperNormal objGlobalHelperNormal;
    public Validation objValidation;
    boolean isThai = true;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    //  public DBHelper mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        objGlobalHelperNormal = GlobalHelperNormal.getInstance();
        objValidation = Validation.getInstance();
        pref = getSharedPreferences(Constants.OAUTH_PREF, MODE_PRIVATE);
        editor = pref.edit();
        String locale = pref.getString(Constants.LOCALE, "");
        if (locale.equals("th")) {
            isThai = true;
            FrontEngine.getInstance().changeLang("th", getApplicationContext());
            editor.putString(Constants.LOCALE, "th").commit();
        } else if (locale.equals("") || locale.equals("en")) {
            isThai = false;
            FrontEngine.getInstance().changeLang("en", getApplicationContext());
            editor.putString(Constants.LOCALE, "en").commit();
        }
        //    mDatabase = new DBHelper(this);
    }
}
