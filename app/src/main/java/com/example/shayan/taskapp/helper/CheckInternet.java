package com.example.shayan.taskapp.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Talib on 8/28/2015.
 */

public class CheckInternet {

	static ConnectivityManager connectivity;
	static NetworkInfo activeNetwork;
	static CheckInternet checkInternet;
	static boolean check = false;

	public static CheckInternet getInstance(){

		if(checkInternet == null){
			checkInternet = new CheckInternet();
		}
		return checkInternet;
	}


	public void internetCheckTask(Context context, final ConnectionCallBackInternet callBackInternet){

		connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		activeNetwork = connectivity.getActiveNetworkInfo();


		if (activeNetwork != null) {

			if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI){
				check = true;
			}else{
				if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
					check = true;
				}
			}


		}else{
			check = false;
		}

		callBackInternet.intenetConnected(check);

	}

	/*AsyncTask<Boolean, Boolean, Boolean> task;
        public AsyncTask internetCheckTask(final Context context, final ConnectionCallBackInternet callBackInternet)
        {
            task = new AsyncTask<Boolean, Boolean, Boolean>(){
                boolean isConnected =false;
                @Override
                protected Boolean doInBackground(Boolean[] params) {
                    isConnected = isConnectingToInternet(context);
                    return null;
                }

                @Override
                protected void onPostExecute(Boolean o) {
                    super.onPostExecute(o);
                    callBackInternet.intenetConnected(isConnected);
                }
            };

            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

            return task;
        }*/
	public static boolean isConnectingToInternet(Context context) {
		boolean connected = false;
		ConnectivityManager CManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo NInfo = CManager.getActiveNetworkInfo();
		if (NInfo != null && NInfo.isConnectedOrConnecting()) {
			try {
				if (InetAddress.getByName("digitonics.com").isReachable(7000)) {
					// host reachable
					connected = true;
				} else {
					// host not reachable
					connected = false;

				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return connected;
	}
	public interface ConnectionCallBackInternet {
		void intenetConnected(boolean status);
	}
}
