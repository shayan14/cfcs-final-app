package com.example.shayan.taskapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.shayan.taskapp.MainActivity;
import com.example.shayan.taskapp.R;
import com.example.shayan.taskapp.helper.FrontEngine;
import com.example.shayan.taskapp.helper.GlobalHelperNormal;


public class AboutFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    String lang=null;
    private Button ChangeLanguage;
    android.content.res.Configuration config;
    Context context;

    TextView txtInfo;

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_about, container,
                false);
        context = getActivity();
        initialWork();
        initializeControls();
        setOnClickListeners();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.conatct), View.VISIBLE, R.mipmap.icon_menu, View.GONE, 0);

    }


    private void initializeControls() {
        txtInfo = (TextView) rootView.findViewById(R.id.txtInfo);
        ChangeLanguage = (Button) rootView.findViewById(R.id.changelang);
//        config.loc
    }


    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);

        ChangeLanguage.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    public void setData() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).openAndCloseDrawer(true);
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
            case R.id.changelang:
                break;
        }
        }

}
