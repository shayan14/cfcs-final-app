package com.example.shayan.taskapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.shayan.taskapp.MainActivity;
import com.example.shayan.taskapp.R;


public class EmptyFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;

    public EmptyFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
 //       rootView = inflater.inflate(R.layout.fragment_about, container,
   //             false);

        //initialWork();
        initializeControls();
        setOnClickListeners();
        return rootView;
    }



 /*   public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.about), View.VISIBLE, R.mipmap.icon_menu, View.GONE, 0);
    }
*/
    private void initializeControls() {
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).openAndCloseDrawer(true);
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
        }
    }





}
