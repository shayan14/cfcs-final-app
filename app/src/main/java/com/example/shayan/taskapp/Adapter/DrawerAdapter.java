package com.example.shayan.taskapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shayan.taskapp.R;

import java.util.ArrayList;

public class DrawerAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<String> IconName;
    private ArrayList<Integer> IconIMG;


    public DrawerAdapter(Context context,ArrayList<String> Name,ArrayList<Integer> IconImg ) {
        this.mInflater = LayoutInflater.from(context);
        this.IconName= Name;
        this.IconIMG=IconImg;
        this.context=context;
    }





    @Override
    public int getCount() {
        return IconName.size();
    }

    @Override
    public Object getItem(int position) {
        return IconName.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.menu_item1, parent, false);
        }

        // get current item to be displayed

        // get the TextView for item name and item description
        TextView textView = (TextView)
                convertView.findViewById(R.id.text1);
        ImageView imageView= (ImageView)
                convertView.findViewById(R.id.img1);

        //sets the text for item name and item description from the current item object
        textView.setText(IconName.get(position));
        imageView.setImageResource(IconIMG.get(position));

        // returns the view for the current row
        return convertView;
    }
}
