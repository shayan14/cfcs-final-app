package com.example.shayan.taskapp.model;


public class ProfileModel {

    int followers,following,propic;
    String name, bio, status,email,twitter,phone;


    public ProfileModel(){

    }


    public ProfileModel(int following, int followers, int propic,  String name, String bio, String status,String email, String twitter, String phone) {
        this.followers= followers;
        this.following= following;
        this.bio= bio;
        this.propic = propic;
        this.email = email;
        this.name = name;
        this.twitter = twitter;
        this.phone= phone;
        this.status=status;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    public int getPropic() {
        return propic;
    }

    public void setPropic(int propic) {
        this.propic = propic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
