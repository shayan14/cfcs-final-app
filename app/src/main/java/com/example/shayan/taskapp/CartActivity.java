package com.example.shayan.taskapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shayan.taskapp.Config.Config;
import com.example.shayan.taskapp.helper.FrontEngine;
import com.example.shayan.taskapp.model.ItemModel;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.example.shayan.taskapp.helper.FrontEngine.pictureImagePath;


public class CartActivity extends AppCompatActivity implements View.OnClickListener{
    private ArrayList<ItemModel> items;
    private static final int PAYPAL_REQUEST_CODE=7171;
    private static PayPalConfiguration config= new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Config.PAYPAL_CLIENT_ID);

    private RecyclerView mRecyclerView;
    private TextView mPriceText;
    private String apple=null;
    private static String amount = null;
    Button PaymentBtn;
    ImageView productImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        initializeControls();
        setData();
        setRecyclerView();

    }



    private void processPayment(){

        PayPalPayment payPalPayment= new PayPalPayment(new BigDecimal(String.valueOf(amount)), "Rs.","Pay Via Paypal",PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent= new Intent(this, com.paypal.android.sdk.payments.PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payPalPayment);
        startActivityForResult(intent,PAYPAL_REQUEST_CODE);
    }

    private void setData() {
        String value = getIntent().getExtras().getString("key");
        apple=value;
       items= new ArrayList<>();
        ItemModel ariel = new ItemModel();
        ItemModel ariel500g = new ItemModel();
        ItemModel dettol = new ItemModel();
        ItemModel vanilla = new ItemModel();
        ItemModel pepsi= new ItemModel();
        ItemModel silkyblack = new ItemModel();
        ItemModel alluring= new ItemModel();
        ItemModel beautiful= new ItemModel();
        ItemModel nights= new ItemModel();
        ItemModel fadeo= new ItemModel();
        ItemModel olive= new ItemModel();
//

        ariel=new ItemModel("Ariel",1, 500,"1kg",1);
        ariel500g=new ItemModel("Ariel",1, 250,"500g",1);
        dettol=new ItemModel("Dettol",300, 700,"250ml",1);
        vanilla=new ItemModel("Vanilla Wafer",1,100,"12pc",1);
        pepsi=new ItemModel("Pepsi Soda",1,90,"12pc",1);
        silkyblack=new ItemModel("Head & Shoulders Shampoo",1,200,"400ml",1);
        alluring=new ItemModel("Enchanteur",1,150,"200g",1);
        beautiful=new ItemModel("Revlon Hair Color",1,120,"50g",1);
        nights=new ItemModel("Mortien Peaceful Nights",1,120,"12pcs",1);
        fadeo=new ItemModel("Fa Deodrant",1,75,"1 Pcs",1);
                olive=new ItemModel("Vatika Olive Oil",1,160,"100ml",1);

        if(apple.contains("Ariel")){
            items.add(ariel);
            }
        if(apple.contains("Dettol")){
            items.add(dettol);
        }
        if(apple.contains("wafer")){
            items.add(vanilla);
        }
        if(apple.contains("Pepsi")){
            items.add(pepsi);
        }
        if(apple.contains("ariel500g")){
            items.add(ariel500g);
        }
        if(apple.contains("oliveOil")){
            items.add(olive);
        }
        if(apple.contains("deo")){
            items.add(fadeo);
        }
        if(apple.contains("haircolor")){
            items.add(beautiful);
        }
        if(apple.contains("Powder")){
            items.add(alluring);
        }
        if(apple.contains("Silky Black")){
            items.add(silkyblack);
        }
        if(apple.contains("Mortine")){
            items.add(nights);
        }

    }

    private void setRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ItemsAdapter adapter = new ItemsAdapter(items);
        mRecyclerView.setAdapter(adapter);
        setData();
        adapter.notifyDataSetChanged();
        int price = 0;
        for (ItemModel i : items) {
            price += i.getPrice() * i.getCount();
        }
        mPriceText.setText("Rs " + String.valueOf(price));
        amount=mPriceText.getText().toString();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.payment:
                processPayment();
                break;



        }
    }


    private class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {
        private List<ItemModel> item;

        ItemsAdapter(List<ItemModel> itemList) {
            item = new ArrayList<>();
            this.item = itemList;
        }

        @NonNull
        @Override
        public ItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_cart, parent, false);
            return new ItemsAdapter.ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final ItemsAdapter.ViewHolder holder, final int position) {
            final ItemModel itm = item.get(position);
            holder.name.setText(itm.getName());
            holder.price.setText(String.valueOf(itm.getPrice()));
            holder.quantity.setText(itm.getVariant());
            holder.count.setText(String.valueOf(itm.getCount()));

            holder.add.setOnClickListener(v -> {
                itm.setCount(itm.getCount() + 1);
                holder.count.setText(String.valueOf(itm.getCount()));
            });

            holder.remove.setOnClickListener(v -> {
                if (!(itm.getCount() <= 0))
                    itm.setCount(itm.getCount() - 1);
                if (itm.getCount() == 0) {
                    item.remove(holder.getAdapterPosition());
                    notifyDataSetChanged();
                } else holder.count.setText(String.valueOf(itm.getCount()));
            });

        }

        @Override
        public int getItemCount() {
            return item.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            private TextView name, price, quantity, count;
            private ImageView add, remove;


            ViewHolder(View itemView) {
                super(itemView);
                name = itemView.findViewById(R.id.from_name);
                price = itemView.findViewById(R.id.price_text);
                quantity = itemView.findViewById(R.id.weight_text);
                count = itemView.findViewById(R.id.cart_product_quantity_tv);
                add = itemView.findViewById(R.id.cart_plus_img);
                remove = itemView.findViewById(R.id.cart_minus_img);
            }
        }
    }


    private void initializeControls() {
        PaymentBtn=findViewById(R.id.payment);
        mRecyclerView = findViewById(R.id.cart_list);
        mPriceText = findViewById(R.id.price);
        productImage=findViewById(R.id.list_image);


    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == PAYPAL_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if(confirmation != null){
                    try{
                        String paymentDetails = confirmation.toJSONObject().toString(4);
                        startActivity(new Intent(this,PaymentDetails.class)
                                .putExtra("PaymentDetails",paymentDetails)
                                .putExtra("PaymentAmount",amount)
                        );
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
        }else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
            Toast.makeText(this, "Invalid", Toast.LENGTH_SHORT).show();
    }
}

