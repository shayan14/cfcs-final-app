package com.example.shayan.taskapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.shayan.taskapp.MainActivity;
import com.example.shayan.taskapp.R;
import com.example.shayan.taskapp.helper.FrontEngine;
import com.example.shayan.taskapp.model.UserModel;

import java.util.ArrayList;


public class ProfileFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    UserModel obj;
    Context context;
    ImageView profileImg;
    TextView UserName,UserBio,UserFollowers,UserFollowing,UserEmail,UserPhone,UserTwitter;
    private ArrayList<UserModel> arrListCategories;


    public ProfileFragment () {

        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        obj= (UserModel) getArguments().getSerializable("123");
        initialWork();
        initializeControls();
        setOnClickListeners();
        initializeValues();
        return rootView;
    }

    private void initializeValues() {
        String Name=obj.getName();
        String Bio=obj.getBio();
        String Followers=obj.getFollowers();
        String Following=obj.getFollowing();
        String Email=obj.getEmail();
        String Phone=obj.getPhone();
        String Twitter=obj.getTwitter();
        UserName.setText(Name);
        UserBio.setText(Bio);
        UserFollowers.setText(Followers);
        UserFollowing.setText(Following);
        UserEmail.setText(Email);
        UserPhone.setText(Phone);
        UserTwitter.setText(Twitter);
        String imgurl = obj.getPropic();
        Glide.with(getActivity()).load(imgurl).into(profileImg);




    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.profile), View.VISIBLE, R.mipmap.back, View.GONE, 0);
    }
    private void initializeControls() {
    UserEmail=(TextView) rootView.findViewById(R.id.tv_email);
    UserName=(TextView) rootView.findViewById(R.id.tv_name);
    UserPhone=(TextView) rootView.findViewById(R.id.tv_mob_no);
    UserFollowers=(TextView) rootView.findViewById(R.id.tv_followers);
    UserFollowing=(TextView) rootView.findViewById(R.id.tv_following);
    UserTwitter=(TextView) rootView.findViewById(R.id.tv_twitter_hanle);
    UserBio=(TextView) rootView.findViewById(R.id.tv_occupation);
    UserFollowing=(TextView) rootView.findViewById(R.id.tv_following);
    profileImg=(ImageView) rootView.findViewById(R.id.iv_propic);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).ivMenu1.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivMenu1:

                ((MainActivity) getActivity()).replaceFragment(new FriendsFragment(),getActivity());
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
        }
    }





}
